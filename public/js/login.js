/**
 * Created by alexb on 05/07/18.
 */
$(document).ready(function () {
  console.log("DOM ");

  $("#login_form").submit(function (e) {
    e.preventDefault();

    var $inputs = $('#login_form :input');

    var values = {};
    $inputs.each(function () {
      if (this.name)
        values[this.name] = $(this).val();
    });

    console.log(values);

    $.post("/api/login", values, function () {
      console.log("success");
    }).done(function (data) {

      if (values.email == data) {
        window.top.location.href = "/";
      }

    }).fail(function () {
      alert("error");
    })

  });

});