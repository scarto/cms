/**
 * Created by alexb on 05/07/18.
 */

$(document).ready(function () {
  console.log("DOM ");
  $("#btnSubmit").click(function (event) {
    //stop submit the form, we will post it manually.
    event.preventDefault();
    doAjax();
  });

  jQuery.fn.extend({
    size: function() {
      return $(this).length;
    }
  });

  $('#datetime').datepicker({});

});

function doAjax() {
  var form = $('#videoForm')[0];
  var data = new FormData(form);

  console.log(data);

  $.ajax({
    type: "POST",
    enctype: 'multipart/form-data',
    url: "/api/upload/video",
    data: data,
    processData: false, //prevent jQuery from automatically transforming the data into a query string
    contentType: false,
    cache: false,
    success: function (data) {
      console.log(data);
      $("#confirmMsg").text(data);
    },
    error: function (e) {
      $("#confirmMsg").text(e.responseText);
    }
  });
}