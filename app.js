var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var favicon = require('serve-favicon');
var bodyParser = require('body-parser');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(logger('dev'));
app.use(express.json());
app.use(cookieParser());
app.use(bodyParser.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(favicon(__dirname + '/public/favicon.png'));
app.use(express.static(path.join(__dirname, 'public')));
global.__basedir = __dirname;

//Routes
app.all('/', function(req, res, next) {

  console.log('======== ROOT ROUTE ALL ========');
  console.log('cookies:',req.cookies);
  console.log('route:',req.originalUrl);
  var cookie = req.cookies.epicUserSession || '{}';

  cookie = JSON.parse(cookie);

  if (Object.keys(cookie).length === 0) {

    res.redirect('/login');
  } else {
    console.log('cookie exists');
    req.userCookieJson = cookie;
    next();
  }

  //TODO: better winston for logger backend -> http://blog.soshace.com/ru/2016/11/24/node-js-conf-templating-pt1/

});

var indexRouter = require('./routes/index');
var loginRouter = require('./routes/login');
var loginApiRouter = require('./routes/api/login');
var usersRouter = require('./routes/users');
var uploadRouter = require('./routes/upload');
var uploadApiRouter = require('./routes/api/upload');
var videolibraryRouter = require('./routes/videolibrary');

app.use('/', indexRouter);
app.use('/login', loginRouter);
app.use('/api/login', loginApiRouter);
app.use('/users', usersRouter);
app.use('/upload', uploadRouter);
app.use('/api/upload', uploadApiRouter);
app.use('/videolibrary', videolibraryRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
