var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  console.log('======== INDEX ROUTE ========');
  var cookie = req.cookies.epicUserSession;
  cookie = JSON.parse(cookie);
  var user = cookie.user;

  console.log(user);

  res.render('index', {
    title: 'Epic',
    name : user.name,
    email : user.email
  });
});

module.exports = router;
