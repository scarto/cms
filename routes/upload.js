/**
 * Created by alexb on 05/07/18.
 */
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  console.log('======== UPLOAD ROUTE ========');
  var cookie = req.cookies.epicUserSession || '{}';
  cookie = JSON.parse(cookie);

  if (cookie && cookie.user){
    var user = cookie.user;

    console.log(user);

    res.render('upload', {
      title: 'Epic',
      name : user.name,
      email : user.email
    });
  } else {
    res.redirect('/login')
  }
});

module.exports = router;
