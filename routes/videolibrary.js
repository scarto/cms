/**
 * Created by alexb on 05/07/18.
 */
var express = require('express');
var dataJson = require('../public/upload/json/data.json');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  console.log('======== VIDEOLIBRARY ROUTE ========');
  var cookie = req.cookies.epicUserSession || '{}';
  cookie = JSON.parse(cookie);

  if (cookie && cookie.user){
    var user = cookie.user;

    console.log(user, dataJson);

    res.render('videolibrary', {
      title: 'Epic',
      name : user.name,
      email : user.email,
      dataJson : dataJson
    });
  } else {
    res.redirect('/login')
  }
});

module.exports = router;

