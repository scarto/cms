var express = require('express');
var path = require('path');
var router = express.Router();
var _ = require('underscore');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post("/", function (req, res) {
  console.log('login post');
  var email = req.body.email;
  var pwd = req.body.pwd;
  var users = require('../../config/users.json').users;

  var user = _.find(users, function (obj) { return obj.email === email; });
  console.log(email, user);

  if (user && user.pwd == pwd){
    var cookieJson = {
      uuid : guid(),
      user : user
    };
    res.cookie('epicUserSession',JSON.stringify(cookieJson), { maxAge: 60 * 60 * 1000, httpOnly: false });
    console.log('cookie created: ', cookieJson.uuid);
    res.send(user.email);
  } else {
    res.send('wrong user');
  }
});

module.exports = router;

// http://javasampleapproach.com/node-js/multer/nodejs-express-upload-text-fields-multipartfile-with-multer-jquery-ajax-bootstrap-4

function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
}
