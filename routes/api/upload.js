var express = require('express');
var multer = require('multer');
var path = require('path');
var fs = require('fs');
var router = express.Router();
var jsonfile = require('jsonfile');
var dataJson = require('../../public/upload/json/data.json');

var storage = multer.diskStorage({
  // destination
  destination: function (req, file, cb) {
    // cb(null, './uploads/')
    cb(null, path.join(__basedir, '/public/upload/video'));
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '_' + file.originalname);
  }
});

var upload = multer({ storage: storage });


/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post("/video", upload.single("videoFile"), function (req, res) {
  var file = req.file;

  var timeStamp = file.filename.split('_')[0];
  var originalName = file.originalname;

  var applicationform = {
    name: req.body.name,
    description: req.body.description,
    datetime: req.body.datetime,
    uploaded : timeStamp,
    originalName : originalName,
    videoDir : '/public/upload/video',
    singleDataDir : '/public/upload/data/',
    dataDir : '/public/upload/json/',
    file: req.file
  };

  dataJson.video.push(applicationform);

  jsonfile.writeFile(path.join(__basedir, '/public/upload/json/data.json'), dataJson, {spaces: 2, EOL: '\r\n'}, function (err) {
    console.error(err)
  });

  var singleData = path.join(__basedir, '/public/upload/data/' + timeStamp + '.json');

  jsonfile.writeFile(singleData, applicationform, {spaces: 2, EOL: '\r\n'}, function (err) {
    console.error(err)
  });


  var data = (JSON.stringify(applicationform, null, 4));
  console.log('--- api/upload ---');
  console.log(data);
  console.log('------------------');


  res.json(applicationform);
});

module.exports = router;


// http://javasampleapproach.com/node-js/multer/nodejs-express-upload-text-fields-multipartfile-with-multer-jquery-ajax-bootstrap-4