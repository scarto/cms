/**
 * Created by alexb on 05/07/18.
 */
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {

  console.log('======== LOGIN ROUTE ========');
  var cookie = req.cookies.epicUserSession;

  if (cookie) {
    res.redirect('/');
  } else {
    res.render('login', { title: 'Express' });
  }
});

module.exports = router;
