/**
 * Created by alexb on 10/05/18.
 */
var mainJsonData = {};
var timeRangeTimeout = 555;
var timeRangeTimeoutFlag = true;
var floor = 0;
var logPrev = '1';
var rangeValue = 8;
var uniqueUsers = 0;

var $amount = {};
var $amount2 = {};
var $status = {};
var $slider = {};
var $slider2 = {};

$(document).ready(function () {
  var query = window.location.search.substring(1);
  var query_parsed = parse_query_string(query);

  floor = query_parsed.floor || floor;
  var demo = query_parsed.demo || '1';
  var filename = query_parsed.filename || '';

  getJson(floor, demo, filename);

  if(filename)
    $status.text('Data JSON uploaded!');

//    console.log(query_parsed.floor)

  $status = $("#status");
  $slider = $('#flat-slider');
  $slider2 = $('#flat-slider2');
  $amount = $( "#amount" );
  $amount2 = $( "#amount2" );


  $('#sel1').change(function(){
    logPrev = $(this).val();
    console.log('logPrev:', logPrev);
    d3Json(mainJsonData, logPrev);
  });

  $('.radio-hrs').change(function(){
    rangeValue = Number($(this).val());
    var val = $slider2.slider( "value" );
    $slider.slider( "values", [val, val + rangeValue*3600] );
    var startTime = timeConverter(val, true);
    var endTime = timeConverter(val + rangeValue*3600, true);
    $amount2.text(startTime + ' + ' + rangeValue +" hrs");
    convertDataToD3(val, val + rangeValue*3600);
  });

  $slider.slider({
    orientation: 'horizontal',
    range:       true,
    min: 0,
    max: 10000,
    step: 1,
    values: [0, 1000],
    slide: function( event, ui ) {
      var startTime = timeConverter(ui.values[ 0 ], true);
      var endTime = timeConverter(ui.values[ 1 ], true);
      $amount.text(startTime + " - " + endTime);
      $( "#count" ).html('<span>Visitors: </span>' + uniqueUsers);
      convertDataToD3(ui.values[ 0 ], ui.values[ 1 ]);
    }
  });

  $slider2.slider({
    orientation: 'horizontal',
    range:       false,
    min: 0,
    max: 10000,
    value: 0,
    step: 1,
    slide: function( event, ui ) {
      var val = rangeValue*3600;
      var startTime = timeConverter(ui.value, true);
      var endTime = timeConverter(ui.value + val, true);
      $slider.slider( "values", [ui.value, ui.value + val] );
      $amount.text(startTime + " - " + endTime);
      $amount2.text(startTime + ' + ' + rangeValue +" hrs");
      $( "#count" ).html('<span>Visitors: </span>' + uniqueUsers);
      convertDataToD3(ui.value, ui.value + val);
    }
  });

  $('.container .row .col-sm').first().css({"background": "url('/extern/resized10p/00" + floor +".jpg')", "background-size": "100% 100%"});

  function getJson(floor, demo, filename, range) {

    console.log('GET DATA: ',floor, demo, filename);

    $.getJSON("/heatmap/json?floor=" + floor + '&demo=' + demo + '&filename=' + filename, function (data) {
      mainJsonData = data;
      console.log(data);
      d3Json(data, logPrev);
      setSliderRange(data);
      d3JsonRoadMap(mainJsonData, logPrev);
    });
  }

  function setSliderRange(data){
    var timeRange = data.timeRange;
    $slider.slider( "option", "min", timeRange.min );
    $slider.slider( "option", "max", timeRange.max );
    $slider.slider( "values", [ timeRange.min, timeRange.max ] );

    var startTime = timeConverter($slider.slider( "values", 0 ), true);
    var endTime = timeConverter($slider.slider( "values", 1 ), true);

    $slider2.slider( "option", "min", timeRange.min );
    $slider2.slider( "option", "max", timeRange.max );

    $amount.text(startTime + " - " + endTime);
    $amount2.text(startTime);
    $( "#count" ).html('<span>Visitors: </span>' + uniqueUsers);
  }

  $('.heatmap-btn').click(function(){
    demo = $(this).val();
    // console.log(demo);
    $('.dropdown-floor').each(function(index) {
      var href = $( this ).attr("href");
      var parts = href.split('demo=');
      href = parts[0] + 'demo=' + demo;
      $( this ).attr("href", href);
    });
    getJson(floor, demo)
  });

  $('.floor-count').click(function () {
    var floorMenu = this.dataset.value || '0';
    console.log(floorMenu)
  });

  $('#uploadForm').submit(function() {
    $status.empty().text("File is uploading...");

    $(this).ajaxSubmit({
      error: function(xhr) {
//          status('Error: ' + xhr.status);
        $status.empty().html('Error: ' + xhr.status);
      },

      success: function(response) {
        console.log(response);
      }
    });

    return false;
  });

  // $('#canvas-roadmap').click(function () {
  //   d3JsonRoadMap(mainJsonData, logPrev)
  // })

});

function d3JsonRoadMap(_data, _log) {

  var log = logPrev;
  var data = mainJsonData;

  var volcano = {
    width: 1,
    height: 256,
  };
  var unitScale = data.unitScale;

  var i0 = d3.interpolateHsvLong(d3.rgb(0, 0, 128), d3.rgb(0, 0, 255));
  var i1 = d3.interpolateHsvLong(d3.rgb(0, 0, 255), d3.rgb(0, 255, 255));
  var i2 = d3.interpolateHsvLong(d3.rgb(0, 255, 255), d3.rgb(255, 255, 0));
  var i3 = d3.interpolateHsvLong(d3.rgb(255, 255, 0), d3.rgb(255, 0, 0));
  var i4 = d3.interpolateHsvLong(d3.rgb(255, 0, 0), d3.rgb(128, 0, 0));

  var interpolateTerrain = function (t) {
    if (t<0.125)
      return i0(t*8);
    else if (t<0.375)
      return i1((t-0.125)*4);
    else if (t<0.625)
      return i2((t-0.375)*4);
    else if (t<0.875)
      return i3((t-0.625)*4);
    else
      return i4((t-0.875)*8);
  };

  var color;

  // var maxValBin = _data && _data.maxBin ? _data.maxBin : 1;
  var maxValBinScale = _data && _data.maxBin ? _data.maxBin*unitScale : 1;

  // if (log == 1) {
  //   maxValBin = Math.log(1+maxValBin)
  // } else if (log == 2) {
  //   maxValBin = Math.log(1 + Math.log(1+maxValBin))
  // }

  console.log('maxValBinScale = ' + maxValBinScale);

  color = d3.scaleSequential(interpolateTerrain).domain([0, maxValBinScale]);
//      if (error) throw error;


  var n = volcano.width,
      m = volcano.height;

  var canvas = d3.select("#canvas-roadmap")
      .attr("width", n)
      .attr("height", m);

  var context = canvas.node().getContext("2d");
  var image = context.createImageData(n, m);


  // for (var i = 0, l = 0; i < 256; ++i, l += 4) {
  for (var i = 256, l = 0; i > 0; --i, l += 4) {

    var c;
    var val=i*maxValBinScale/255;

    c = d3.rgb(color(val));

    image.data[l + 0] = c.r;
    image.data[l + 1] = c.g;
    image.data[l + 2] = c.b;
    // image.data[l + 3] = 255;
    image.data[l + 3] = 255 * val / maxValBinScale;
  }

  context.putImageData(image, 0, 0);

  if (maxValBinScale < 2) {
    $('.span-poadmap').hide()
  } else {
    $('.span-poadmap').show()
  }

  updateRoadMap(maxValBinScale);
  // $( "#count" ).html('<span>Visitors: </span>' + mainJsonData.uniqueUsers);
  $( "#count" ).html('<span>Visitors: </span>' + uniqueUsers);
}

function updateRoadMap(maxValBinScale){

  // console.log('updateRoadMap', maxValBinScale);

  var digits = maxValBinScale.toString().length;
  var ten = (Math.pow(10, digits-1));

  var num = (Math.floor(maxValBinScale/ten))*ten;

  var ticks = niceTicks(maxValBinScale, logPrev);
  // console.log(num, ticks);

  var numScale = Math.log(1+num)/Math.log(1+maxValBinScale);
  var numScale2 = Math.log(1+ticks[1])/Math.log(1+maxValBinScale);

  var height = $('#canvas-roadmap').height();
  var floatRigth = height- (height - height*numScale);
  var floatRigth2 = height - (height - height*numScale2);

  $('.span-poadmap:last-child').html(num + ' ' + '<span class="arrow-span">&#8594;</span>').css({
    "bottom": floatRigth - 8 + 'px'
  });

  $('.span-poadmap:nth-child(2)').html(ticks[1] + ' ' + '<span class="arrow-span">&#8594;</span>').css({
    "bottom": floatRigth2 - 8 + 'px'
  });

  $('#roadmap-max').html('max : ' + '<span class="max-span">' + (maxValBinScale > 1 ? maxValBinScale : 0) + '</span>' + ' (accumulated number of pings per square 1/4 meter per time)');
}

function niceTicks(max, logState) {
  var ticks = [0];

  console.log ('in niceTicks(). max=' + max);
  var biggestExp10 = Math.floor(Math.log10(max));
  var biggestPower10 = Math.pow(10,biggestExp10);
  var biggestNice = biggestPower10 * Math.floor(max / biggestPower10);

  var iC = Math.floor((biggestExp10+1)/2);
  ticks.push(Math.pow(10,iC));
  ticks.push(biggestNice);

  return (ticks);
}

function d3Json(data, log) {
  var volcano = {
    width: data.dim.x,
    height: data.dim.y,
    values: data.data
  };

  var i0 = d3.interpolateHsvLong(d3.rgb(0, 0, 128), d3.rgb(0, 0, 255));
  var i1 = d3.interpolateHsvLong(d3.rgb(0, 0, 255), d3.rgb(0, 255, 255));
  var i2 = d3.interpolateHsvLong(d3.rgb(0, 255, 255), d3.rgb(255, 255, 0));
  var i3 = d3.interpolateHsvLong(d3.rgb(255, 255, 0), d3.rgb(255, 0, 0));
  var i4 = d3.interpolateHsvLong(d3.rgb(255, 0, 0), d3.rgb(128, 0, 0));

  var interpolateTerrain = function (t) {
    if (t<0.125)
      return i0(t*8);
    else if (t<0.375)
      return i1((t-0.125)*4);
    else if (t<0.625)
      return i2((t-0.375)*4);
    else if (t<0.875)
      return i3((t-0.625)*4);
    else
      return i4((t-0.875)*8);
  };

  var color;
  var unitScale = data.unitScale;
  var maxValBin = data.maxBin*unitScale;
  var maxValBin2 = data.maxBin*unitScale;

  if (log == 1) {
    maxValBin = Math.log(1+maxValBin)
  } else if (log == 2) {
    maxValBin = Math.log(1 + Math.log(1+maxValBin))
  }

  console.log('maxValBin = ' + maxValBin);


  color = d3.scaleSequential(interpolateTerrain).domain([0, maxValBin]);
//      if (error) throw error;


  var n = volcano.width,
      m = volcano.height - 1;

  var canvas = d3.select("canvas")
      .attr("width", n)
      .attr("height", m);

  var context = canvas.node().getContext("2d");
  var image = context.createImageData(n, m);

  for (var j = m, l = 0; j > 0; --j) {
    for (var i = 0; i < n; ++i, l += 4) {

      var c;
      var val=volcano.values[i][j]*unitScale;

      if (log == 1) {
        val = Math.log(1+val)
      } else if (log == 2) {
        val = Math.log(1 + Math.log(1+val))
      }

      c = d3.rgb(color(val));

      image.data[l + 0] = c.r;
      image.data[l + 1] = c.g;
      image.data[l + 2] = c.b;
      // image.data[l + 3] = 255;
      image.data[l + 3] = 255 * val / maxValBin;
    }
  }

  context.putImageData(image, 0, 0);
}

function timeConverter(UNIX_timestamp, short){
  var a = new Date(UNIX_timestamp * 1000);
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = a.getHours();
  var min = a.getMinutes();
  var sec = a.getSeconds();
  var time = date + ' ' + month + ' ' + year + ' ' + (hour<10?'0'+ hour:hour) + ':' + (min<10?'0'+min:min) + ':' + sec ;
  var timeShort = date + ' ' + month + ' ' + (hour<10?'0'+ hour:hour) + ':' + (min<10?'0'+min:min);

  if (short)
    return timeShort;

  return time;
}

function parse_query_string(query) {
  var vars = query.split("&");
  var query_string = {};
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split("=");
    // If first entry with this name
    if (typeof query_string[pair[0]] === "undefined") {
      query_string[pair[0]] = decodeURIComponent(pair[1]);
      // If second entry with this name
    } else if (typeof query_string[pair[0]] === "string") {
      var arr = [query_string[pair[0]], decodeURIComponent(pair[1])];
      query_string[pair[0]] = arr;
      // If third or later entry with this name
    } else {
      query_string[pair[0]].push(decodeURIComponent(pair[1]));
    }
  }
  return query_string;
}

function convertDataToD3(minTimeR, maxTimeR){

  var data = mainJsonData;
  if (!timeRangeTimeoutFlag)
    return;

  timeRangeTimeoutFlag = false;

  setTimeout(function(){
    timeRangeTimeoutFlag = true;
  }, timeRangeTimeout);

  var initState = {
    "MIN_X": 21.08,
    "MIN_Y": 68.89,
    "MAX_X": 144.63,
    "MAX_Y": 214.78
  };

  var bin = 0.5;

  var dim = {
    x : (Math.round(initState.MAX_X - initState.MIN_X))/bin,
    y : (Math.round(initState.MAX_Y - initState.MIN_Y))/bin
  };

  var dataSet = zeros([dim.x, dim.y]);
  var points = 0;
  var minTime = data.data[0].t;
  var maxTime = data.data[0].t;
  var floorPoints = [];
  var users = [];

  var hours = (maxTimeR - minTimeR)/3600;

  data.floorPoints.forEach(function (point) {
    if (point.f == floor && (point.t >= minTimeR && point.t <= maxTimeR)) {
      var xBin = Math.floor((point.x - Math.floor(initState.MIN_X))/bin);
      var yBin = Math.floor((point.y - Math.floor(initState.MIN_Y))/bin);
      dataSet[xBin][yBin]++;
      points++;
      minTime = point.t < minTime ? point.t : minTime;
      maxTime = point.t < maxTime ? maxTime : point.t;
      floorPoints.push(point);
      users.push(point.i);
    }
  });

  var maxBin = 0;

  for(var i = 0; i < dim.x; i++){
    for(var j = 0; j < dim.y; j++){
      maxBin = maxBin > dataSet[i][j] ? maxBin :dataSet[i][j];
    }
  }

  var d3Data = {
    data : dataSet,
    dim : dim,
    maxBin: maxBin,
    log : 1,
    points : points,
    uniqueUsers: uniqueUsers,
    unitScale : 1/(bin*bin*hours), // scale the dataSet to get --> accumulated number of pings per square meter per hour
    timeRange : {
      min: minTime,
      max: maxTime
    }
  };

  d3Json(d3Data, logPrev);
  d3JsonRoadMap(d3Data, logPrev)

}

function onlyUnique(value, index, self) {
  return self.indexOf(value) === index;
  // usage example:
  // var a = ['a', 1, 'a', 2, '1'];
  // var unique = a.filter( onlyUnique );
}

function zeros(dimensions) {
  var array = [];
  for (var i = 0; i < dimensions[0]; ++i) {
    array.push(dimensions.length == 1 ? 0 : zeros(dimensions.slice(1)));
  }
  return array;
}