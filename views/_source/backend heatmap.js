/**
 * Created by alexb on 23/04/18.
 */
/**
 * Created by alexb on 27/07/17.
 */
var express = require('express');
var router = express.Router();
var appLogger = require('../../server/services/appLogger');
var request = require('request');
var bodyParser = require('body-parser');
var multer = require('multer');

var fs = require('fs');
var path = require('path');

router.use(bodyParser.urlencoded({extended:false}));
router.use(bodyParser.json());

var multerConfig = {

  storage: multer.diskStorage({
    destination: function(req, file, next){
      next(null, path.join(__dirname, '../public/extern/json/'));
    },

    filename: function(req, file, next){
      console.log(file);
      var ext = file.mimetype.split('/')[1];
      // next(null, file.fieldname + '-' + Date.now() + '.'+ext);
      next(null, file.originalname);
    }
  }),

  // filter out and prevent non-image files.
  fileFilter: function(req, file, next){
    if(!file){
      next();
    }

    // const image = true;//true;//file.mimetype.startsWith('application');
    var image = file.mimetype.startsWith('application');
    if(image){
      console.log('file.mimetype CHECK PASS');
      next(null, true);
    }else{
      console.log("file not supported");
      //TODO:  A better message response to user on failure.
      return next();
    }
  }
};

router.all('/', function(req, res, next) {
  console.log("HeatMap ROUTE ALL");
  next();
});

/* GET home page. */
router.get('/', function(req, res, next) {
  appLogger.info('HeatMap ROUTE');
  console.log("HeatMap ROUTE");
  var floor = req.query.floor ? req.query.floor : '0';
  var data = req.query.demo ? String(req.query.demo) : '1';
  var fileName = req.query.filename;



  if (!fileName || fileName == 'undefined') {
    fileName = ''
  }

  var renData = {
    title: "HeatMap ROUTE",
    floor : floor,
    data : data,
    floorCount : 8,
    fileName : fileName
  };

  res.render('heatmap', renData);
});

router.get('/json', function(req, res, next) {
  appLogger.info('HeatMap ROUTE Json');

  var floor = req.query.floor || '0';
  var demo = req.query.demo || '1';
  var fileName = req.query.filename;

  console.log(req.protocol, req.headers.host);
  console.log('floor:', floor, ' demo:', demo, ' filename:', fileName);

  var jsonData = require("./posPathes.json");
  var jsonName = fileName;

  if (!fileName || fileName == 'undefined') {
    switch (demo) {
      case '1':
        jsonName = "posPathes3.json";
        break;
      case '2':
        jsonName = "posBig_part2.json";
        break;
      case '3':
        jsonName = "posDay1.json";
        break;
      default:
        jsonName = "posPathes3.json";
    }
  }

  console.log('jsonName : ', jsonName);

  request.get(req.protocol + '://' + req.headers.host +'/extern/json/' + jsonName, function (error, resp, body) {

    // console.log(resp.body);

    if (!error && resp.statusCode == 200) {
      var data = body ? body : {};
      var obj = JSON.parse(data);
      console.log('Data size:', obj.data.length);

      renderData(obj, jsonName, null)

    } else {
      console.log('ERROR GETTING DATA :', error);
      console.log(req.protocol + '://' + req.headers.host +'/extern/json/' + fileName);
      renderData(jsonData, jsonName, 'ERROR GETTING DATA');
    }
  });

  function renderData(obj, fileName, err){
    var initState = {
      "MIN_X": 21.08,
      "MIN_Y": 68.89,
      "MAX_X": 144.63,
      "MAX_Y": 214.78
    };

    var bin = 0.5;

    var dim = {
      x : (Math.round(initState.MAX_X - initState.MIN_X))/bin,
      y : (Math.round(initState.MAX_Y - initState.MIN_Y))/bin
    };

    var dataSet = zeros([dim.x, dim.y]);
    var points = 0;
    var minTime = obj.data[0].t;
    var maxTime = obj.data[0].t;
    var floorPoints = [];
    var uniqueUsers = 0;
    var users = [];


    obj.data.forEach(function (point) {

      if (point.f == floor) {
        var xBin = Math.floor((point.x - Math.floor(initState.MIN_X))/bin);
        var yBin = Math.floor((point.y - Math.floor(initState.MIN_Y))/bin);
        dataSet[xBin][yBin]++;
        points++;
        minTime = point.t < minTime ? point.t : minTime;
        maxTime = point.t < maxTime ? maxTime : point.t;
        floorPoints.push(point);
        users.push(point.i);
      }

    });

    var maxBin = 0;

    for(var i = 0; i < dim.x; i++){
      for(var j = 0; j < dim.y; j++){
        maxBin = maxBin > dataSet[i][j] ? maxBin :dataSet[i][j];
      }
    }

    res.json({
      data : dataSet,
      floorPoints : floorPoints,
      dim : dim,
      maxBin: maxBin,
      log : 1,
      points : points,
      demo : demo,
      err: err,
      fileName : fileName
  });
  }

});

router.post('/', function (req, res) {
  res.json({post: 'example'})
});

router.post('/upload', multer(multerConfig).single('jsonFile'),function(req, res, err){

  console.log('=========');
  console.log(req.file);
  console.log('=========');

  res.json({
    msg :'Data JSON uploaded!',
    data : req.file
  });
});

module.exports = router;

function zeros(dimensions) {
  var array = [];
  for (var i = 0; i < dimensions[0]; ++i) {
    array.push(dimensions.length == 1 ? 0 : zeros(dimensions.slice(1)));
  }
  return array;
}


//================================OOOOOOOOOOOOLLLLLLLDDDDDDDDDDD  get json

// router.get('/json', function(req, res, next) {
//   appLogger.info('HeatMap ROUTE Json');
//
//   var floor = req.query.floor || '0';
//   var demo = req.query.demo || '1';
//   var fileName = req.query.filename || '';
//
//   console.log('floor:',floor, 'demo:',demo, 'filename:', fileName);
//
//   // var jsonData = require("./posDay1.json");
//   // var jsonData = require("./posBig.json");
//
//   var jsonData = require("./posPathes.json");
//   var jsonDataUrl = 'posPathes.json';
//
//   switch (demo) {
//     case '1':
//       // jsonData = require("./posBig_part1.json");
//       var jsonData = require("./posPathes.json");
//       break;
//     case '2':
//       jsonData = require("./posBig_part2.json");
//       break;
//     case '3':
//       jsonData = require("./posDay1.json");
//       break;
//     default:
//       jsonData = require("./posBig_part1.json");
//   }
//
//   var initState = {
//     "MIN_X": 21.08,
//     "MIN_Y": 68.89,
//     "MAX_X": 144.63,
//     "MAX_Y": 214.78
//   };
//
//   var bin = 0.5;
//
//   var dim = {
//     x : (Math.round(initState.MAX_X - initState.MIN_X))/bin,
//     y : (Math.round(initState.MAX_Y - initState.MIN_Y))/bin
//   };
//
//   var dataSet = zeros([dim.x, dim.y]);
//   var points = 0;
//   var minTime = 0;
//   var maxTime = 0;
//
//   // "t": 1523268595,
//   // "i": 1,
//   // "f": 5,
//   // "x": 101.899462,
//   // "y": 105.658620
//
//
//   jsonData.data.forEach(function (point) {
//
//     if (point.f == floor || demo == 3) {
//       var xBin = Math.floor((point.x - Math.floor(initState.MIN_X))/bin);
//       var yBin = Math.floor((point.y - Math.floor(initState.MIN_Y))/bin);
//       dataSet[xBin][yBin]++;
//       points++
//     }
//
//   });
//
//   var maxBin = 0;
//
//   for(var i = 0; i < dim.x; i++){
//     for(var j = 0; j < dim.y; j++){
//       maxBin = maxBin > dataSet[i][j] ? maxBin :dataSet[i][j];
//     }
//   }
//
//   res.json({
//     data : dataSet,
//     dim : dim,
//     maxBin: maxBin,
//     log : 1,
//     points : points,
//     demo : demo
//   });
//
//
//   //----------------------------------------//
//   // console.log(req.protocol, req.headers.host);
//   //
//   // request.get(req.protocol + '://' + req.headers.host +'/extern/json/posBig.json', function (error, resp, body) {
//   //   if (!error && resp.statusCode == 200) {
//   //     var data = body ? body : {};
//   //     var obj = JSON.parse(data);
//   //     console.log(obj.data.length);
//   //
//   //     renderData(obj)
//   //
//   //   } else {
//   //     console.log('ERROR GETTING DATA :', error);
//   //   }
//   // });
//   //
//   // function renderData(obj){
//   //   var initState = {
//   //     "MIN_X": 21.08,
//   //     "MIN_Y": 68.89,
//   //     "MAX_X": 144.63,
//   //     "MAX_Y": 214.78
//   //   };
//   //
//   //   var bin = 0.5;
//   //
//   //   var dim = {
//   //     x : (Math.round(initState.MAX_X - initState.MIN_X))/bin,
//   //     y : (Math.round(initState.MAX_Y - initState.MIN_Y))/bin
//   //   };
//   //
//   //   var dataSet = zeros([dim.x, dim.y]);
//   //   var points = 0;
//   //
//   //   // "t": 1523268595,
//   //   // "i": 1,
//   //   // "f": 5,
//   //   // "x": 101.899462,
//   //   // "y": 105.658620
//   //
//   //
//   //   // console.log('zzzzzzzzzzzz')
//   //   // console.log((floor), jsonData.data[0].f)
//   //   // console.log((floor)== jsonData.data[0].f)
//   //
//   //   obj.data.forEach(function (point) {
//   //
//   //     if (point.f == floor || demo == 3) {
//   //       var xBin = Math.floor((point.x - Math.floor(initState.MIN_X))/bin);
//   //       var yBin = Math.floor((point.y - Math.floor(initState.MIN_Y))/bin);
//   //       dataSet[xBin][yBin]++;
//   //       points++
//   //     }
//   //
//   //   });
//   //
//   //   var maxBin = 0;
//   //
//   //   for(var i = 0; i < dim.x; i++){
//   //     for(var j = 0; j < dim.y; j++){
//   //       maxBin = maxBin > dataSet[i][j] ? maxBin :dataSet[i][j];
//   //     }
//   //   }
//   //
//   //   res.json({
//   //     data : dataSet,
//   //     dim : dim,
//   //     maxBin: maxBin,
//   //     log : 1,
//   //     points : points,
//   //     demo : demo
//   //   });
//   // }
//
// });